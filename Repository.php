<?php

namespace ContentHub;

use ContentHub\Exception\CouldNotDeleteException;

class Repository
{
    /**
     * @var string
     */
    private $index;

    /**
     * @var Adapter
     */
    private $adapter;

    public function __construct(string $index, Adapter $adapter)
    {
        $this->index = $index;
        $this->adapter = $adapter;
    }

    /**
     * @throws Exception\NoSuchDocumentException
     */
    public function getById(int $id) : Document
    {
        return $this->adapter->getById($this->index, $id);
    }

    /**
     * @throws Exception\NoSuchDocumentException
     */
    public function getByKey(string $key) : Document
    {
        return $this->adapter->getByKey($this->index, $key);
    }

    /**
     * @throws Exception\CouldNotSaveException
     */
    public function save(Document $document) : Document
    {
        return $this->adapter->save($this->index, $document);
    }

    public function delete(Document $document) : bool
    {
        return $this->deleteById($document->getId());
    }

    public function getList()
    {

    }

    public function deleteById(int $id) : bool
    {
        try {
            $this->adapter->deleteById($this->index, $id);
            return true;
        } catch (CouldNotDeleteException $couldNotDeleteException) {
            return false;
        }
    }
}