<?php

namespace ContentHub;

use ContentHub\Exception\NoSuchRepositoryException;

class Client
{
    /**
     * @var array|Mapping[]
     */
    private $mappings = [];

    /**
     * @var array|Repository[]
     */
    private $repositories = [];

    /**
     * @var Adapter
     */
    private $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function addDocumentMapping(Mapping $mapping)
    {
        $this->mappings[$mapping->getCode()] = $mapping;
    }

    /**
     * @throws NoSuchRepositoryException
     */
    public function getRepository(string $code) : Repository
    {
        if (!isset($this->repositories[$code])) {
            if (!isset($this->mappings[$code])) {
                throw new NoSuchRepositoryException(sprintf('No mapping with code %s found', $code));
            }

            $this->repositories[$code] = new Repository(
                $this->mappings[$code]->getIndex(),
                $this->adapter
            );
        }

        return $this->repositories[$code];
    }
}


$client = new \ContentHub\Client(new DummyAdapter());
$repo = $client->getRepository('page');
$repo->save($document);