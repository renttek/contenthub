<?php

namespace ContentHub;

/**
 * Interface Document
 * @package ContentHub
 */
interface Document
{
    /**
     * @return int
     */
    public function getId() : int;

    /**
     * @return string
     */
    public function getKey() : string;

    /**
     * @return string
     */
    public function getLocale() : string;

    /**
     * @return mixed
     */
    public function getField(string $name) : mixed;

    /**
     * @return array|string[]
     */
    public static function getFieldDefinitions() : array;
}
