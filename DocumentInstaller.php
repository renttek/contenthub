<?php

namespace ContentHub;

use ContentHub\Exception\CouldNotInstallException;

/**
 * Interface DocumentInstaller
 * @package ContentHub
 */
interface DocumentInstaller
{
    /**
     * [
     *   'bar' => IntField::class,
     *   'foo' => StringField::class,
     * ]
     *
     * @return array|string[]
     */
    public static function getFieldDefinitions() : array;
}
