<?php

namespace ContentHub;

use ContentHub\Exception\CouldNotDeleteException;
use ContentHub\Exception\CouldNotSaveException;
use ContentHub\Exception\NoSuchDocumentException;

/**
 * Interface Adapter
 * @package ContentHub
 */
interface Adapter
{
    /**
     * @param string $index
     * @param int    $id
     *
     * @return Document
     *
     * @throws NoSuchDocumentException
     */
    public function getById(string $index, int $id) : Document;

    /**
     * @param string $index
     * @param string $key
     *
     * @return Document
     *
     * @throws NoSuchDocumentException
     */
    public function getByKey(string $index, string $key) : Document;

    /**
     * @param string   $index
     * @param Document $document
     *
     * @return Document
     *
     * @throws CouldNotSaveException
     */
    public function save(string $index, Document $document) : Document;

    /**
     * @param string $index
     * @param int    $id
     *
     * @throws CouldNotDeleteException
     */
    public function deleteById(string $index, int $id) : void;
}
