<?php

namespace ContentHub\Exception;

class CouldNotDeleteException extends \Exception
{
}
