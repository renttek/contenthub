<?php

namespace ContentHub\Exception;

class CouldNotSaveException extends \Exception
{
}
