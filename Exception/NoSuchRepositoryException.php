<?php

namespace ContentHub\Exception;

class NoSuchRepositoryException extends \Exception
{
}
