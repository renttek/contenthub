<?php

namespace ContentHub\Exception;

class NoSuchDocumentException extends \Exception
{
}
