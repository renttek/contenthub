<?php

namespace ContentHub;

class Mapping
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $index;

    /**
     * @var string
     */
    private $documentClass;

    /**
     * Mapping constructor.
     *
     * @param string $code
     * @param string $index
     * @param string $documentClass
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $code, string $index, string $documentClass)
    {
        $this->code = $code;
        $this->index = $index;
        $this->documentClass = $documentClass; // TODO: check if class implements \ContentHub\Document
    }

    /**
     * @return string
     */
    public function getCode() : string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getIndex() : string
    {
        return $this->index;
    }

    /**
     * @return string
     */
    public function getDocumentClass() : string
    {
        return $this->documentClass;
    }
}
